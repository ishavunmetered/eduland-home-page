import HomeBanner from "./components/banner/HomeBanner";
import ChooseCountry from "./components/chooseCountry/ChooseCountry";
import Courses from "./components/courses/Courses";
import Testimonials from "./components/testimonials/Testimonials";
import { UniversityPartners } from "./components/universityPatners/UniversityPatners";
import VisaHolders from "./components/visaHolders/VisaHolders";
import VisaSteps from "./components/visaSteps/VisaSteps";
import WhyEduland from "./components/whyEduland/WhyEduland";
import KeepUpdated from "./components/KeepUpdated/KeepUpdated";
export default function Home() {
  return (
    <main>
      <HomeBanner />
      <VisaSteps />
      <ChooseCountry />
      <WhyEduland />
      <Courses />
      <UniversityPartners /> 
      <KeepUpdated />
     <Testimonials />
      <VisaHolders /> 
    </main>
  );
}
