import styles from "./whyEduland.module.scss";

const WhyEduland = () => {
  return (
    <section className="common_margin">
      <div className="container">
        <div className={styles.whyEduland}>
          <h3 className="commonBigHeadingStyle " style={{ textAlign: "center" }}>
            Why <span style={{ color: "#dda23f" }}>Edu</span>land?
          </h3>
          <p className="commonParagraphStyle mb-5">
            Eduland immigration is a Chandigarh-based Visa consultancy firm in
            India that steps forward to facilitate aspiring students, job seekers,
            & individuals seeking a visitor Visa. Our insights are based on
            experience, knowledge, & data, and our organization is based on
            integrity & honesty. Eduland immigration is a registered & licensed
            firm owned & run by Mr. Gurpreet Singh Channa, who has a vast
            experience of over 16 years in the consultancy.
          </p>
          <div>
            <div className={styles.clients}>
              <div>
                <h3 className={styles.clientsCountHeading} >16+</h3>
                <p className={styles.clientsCountPara} >Years of Experience</p>
              </div>
              <div>
                <h3 className={styles.clientsCountHeading}  >10000+</h3>
                <p className={styles.clientsCountPara}>Satisfied Clients</p>
              </div>
              <div style={{ border: "none" }}>
                <h3 className={styles.clientsCountHeading} >50+</h3>
                <p className={styles.clientsCountPara}>  Daily Immigration Queries</p>
              </div>
            </div>
          </div>
          <div className={styles.exploreMoreContainer}>
            <button className="commonButtonStyle">BOOK A CONSULTATION</button>
          </div>
        </div>
      </div>
    </section>
  );
};

export default WhyEduland;
