import Image from "next/image";
import LogoRotator from "../LogoRotator/LogoRotator";
import styles from "./courses.module.scss";

const Courses = () => {
  return (
    <div className={`${styles.courses} common_margin`}>
      <div className="container">
        <div className="row align-items-center">
          <div className="col-lg-7 col-md-12 col-sm-12 px-5">
            <div className={styles.coursesImageContainer} >
              <Image src="/images/cource-img.png" fill={true} alt="Logo" />
            </div>
            {/* <LogoRotator /> */}
          </div>

          <div className="col-lg-5 col-md-12 col-sm-12">
            <h3 className={`commonBigHeadingStyle ${styles.coursesHeading}` }>Courses</h3>
            <p className={`commonParagraphStyle py-3 ${styles.coursesPara}` }>
              Ignite your Future with a Diverse Range of Specialized Courses
              offered by Eduland Immigration. Empower Yourself with Cutting-edge
              Education & Skills Development Programs Tailored to Enhance Your
              Immigration journey & Propel Your Career to New Heights.
            </p>
           <div className={styles.coursesBtn}>
           <button className={`commonButtonStyle `}>Explore our Courses </button>
           </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Courses;
