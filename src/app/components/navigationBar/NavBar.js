"use client";
import { useState } from "react";
import Link from "next/link";
import Image from "next/image";

const NavBar = () => {
  const [navbarOpen, setNavbarOpen] = useState(false);

  const toggleNavbar = () => {
    setNavbarOpen(!navbarOpen);

    if (!navbarOpen) {
      document.body.classList.add("no-overflow");
    } else {
      document.body.classList.remove("no-overflow");
    }
  };
  const closeNavbar = () => {
    setNavbarOpen(false);
  };
  return (
    <div style={{ background: "#153443" }}>
      <header className={styles.sticky_header}>
        <div className={styles.header_bg}>
          <div className="container">
            <nav className={`${styles.nav_bar}`}>
              <div className={styles.nav_bar_img_links}>
                <Link className="" href="/">
                  <Image
                    src="/logo/logo.svg"
                    alt="Eduland Immigration logo"
                    width={157}
                    height={37}
                  />
                </Link>

                {navbarOpen ? (
                  <div className={styles.mobile_nav_bg}>
                    <div className={styles.mobile_nav_links_input}>
                      <ul className={`${styles.mobile_nav_ul}`}>
                        <li>
                          <Link href="/" onClick={closeNavbar}>
                            About Us
                          </Link>
                        </li>
                        <li>
                          <Link href="/" onClick={closeNavbar}>
                            Packages
                          </Link>
                        </li>
                        <li>
                          <Link href="/" onClick={closeNavbar}>
                            Bike Trip
                          </Link>
                        </li>
                        <li>
                          <Link href="/" onClick={closeNavbar}>
                            Destinations
                          </Link>
                        </li>
                        <li>
                          <Link href="/" onClick={closeNavbar}>
                            Contact Us
                          </Link>
                        </li>
                      </ul>
                    </div>
                  </div>
                ) : (
                  <div className={styles.nav_links_input}>
                    <ul className={`${styles.nav_ul}`}>
                      <li>
                        <Link href="/">
                          About Us
                        </Link>
                      </li>
                      <li>
                        <Link href="/">
                          Packages
                        </Link>
                      </li>
                      <li>
                        <Link href="/">
                          Bike Trip
                        </Link>
                      </li>
                      <li>
                        <Link href="/">
                          Destinations
                        </Link>
                      </li>
                      <li>
                        <Link href="/">
                          Contact Us
                        </Link>
                      </li>

                      <li>
                        <button className={styles.book_btn}>Book Consultant</button>
                      </li>
                    </ul>
                  </div>
                )}
                {!navbarOpen ? (
                  <button
                    className={` ${styles.customToggler}`}
                    onClick={toggleNavbar}
                  >
                    <span>
                      <Image
                        src="/icons/menu-icon.svg"
                        alt="menu icon"
                        width={30}
                        height={30}
                      />
                    </span>
                  </button>
                ) : (
                  <button
                    className={` ${styles.customToggler}`}
                    onClick={toggleNavbar}
                  >
                    <span>
                      <Image
                        src="/icons/close-icon.svg"
                        alt="close icon"
                        width={25}
                        height={25}
                      />
                    </span>
                  </button>
                )}
              </div>
            </nav>
          </div>
        </div>
      </header>
    </div>
  );
};

export default NavBar;
