'use client'
import { useState } from 'react';
import Link from 'next/link';
import Image from 'next/image';

const Header = () => {
    const [menuOpen, setMenuOpen] = useState(false);

    const toggleMenu = () => {
        setMenuOpen(!menuOpen);
    };

    return (
        <header className='header_main'>
            <div className='container'>
                <nav className="navbar">
                    <div className="logo">
                        <Link className="" href="/">
                            <Image
                                src="/logo/logo.svg"
                                alt="Eduland Immigration logo"
                                width={157}
                                height={37}
                            />
                        </Link>
                    </div>
                    <div className={`links ${menuOpen ? 'open' : 'close'}`}>
                        {menuOpen === true ? <button className='cross_btn' onClick={toggleMenu}>
                            <Image src='/icons/cross-icon.svg' width={20} height={20} alt='Cancel' />
                        </button> : ''}
                        <Link href="/" >Home</Link>
                        <Link href="/" >About Us</Link>
                        <Link href="/" >Services</Link>
                        <Link href="/" >Courses</Link>
                        <Link href="/" >Blog</Link>
                        <Link href="/" >Contact</Link>
                        <button className='book_appointment_btn'>Book Consultant</button>
                    </div>
                    <div className='hamburger' onClick={toggleMenu}>
                        <div className={`bar ${menuOpen ? 'open' : ''}`}></div>
                        <div className={`bar ${menuOpen ? 'open' : ''}`}></div>
                        <div className={`bar ${menuOpen ? 'open' : ''}`}></div>
                    </div>
                </nav>
            </div>
        </header>
    );
};

export default Header;