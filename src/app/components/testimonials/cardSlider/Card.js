"use client";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import styles from "./card.module.scss"; // Import your styles
import Image from "next/image";
import "./CardSlider.css";

const Card = () => {
  const testimonials = [
    {
      name: "Chashanjot Kaur",
      reviewText: "Guided me all the way",
      text: "My experience with Eduland Immigration Chandigarh is amazing. I'm very grateful to the team for getting my visa timely & for helping & guiding me all the way to getting my visa.",
      imageSrc: "/images/userImages/userImage.png",
    },
    {
      name: "Damanpreet Kaur",
      reviewText: "Will Recommend to others",
      text: "My experience with Eduland immigration is good. Now I recommend to others, that they should definitely visit this place to get a visa as early as possible. I am sure that people can get the right advice from this place.",
      imageSrc: "/images/userImages/userImage.png",
    },
    {
      name: "Surbhi Ghai",
      reviewText: "Excellent Service",
      text: "Eduland immigration services are excellent. Gurpreet Singh sir consults me in a very effective manner. What is good or bad, the scope of the particular course, each and everything. His nature, his way of explaining was quite good.",
      imageSrc: "/images/userImages/userImage.png",
    },
    {
      name: "Khushdeep Kaur",
      reviewText: "Incredible Experience",
      text: "My experience with eduland immigration has been incredible for applying tourist visa of canada as well as we have applied daughters study visa from them. Whole team is supportive and responsible.",
      imageSrc: "/images/userImages/userImage.png",
    },
    
  ];

  const sliderSettings = {
    dots: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    centerPadding: 0,
    className: "center",
    centerMode: true,
    infinite: true,
    autoplay: true,
    slidesToShow: 3,
    speed: 500,
    responsive: [

      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 1
        }
      },

    ]
  };

  return (
    <div>
      <Slider {...sliderSettings} className="testimonail_slider">
        {testimonials.map((testimonial, index) => (
          <div key={index}>
            <div className={`${styles.testimonialCard} testimonial_car_css`} style={{ paddingTop: "50px" }}>
              <div className={styles.testimonialCard_image}>
                <Image
                  src={testimonial.imageSrc}
                  alt={testimonial.name}
                  fill={true}
                />
              </div>
              <h5>{testimonial.name}</h5>
              <h6 style={{textAlign:"center"}}>{testimonial.reviewText}</h6>
              <p>{testimonial.text}</p>
            </div>
          </div>
        ))}
      </Slider>
    </div>
  );
};

export default Card;
