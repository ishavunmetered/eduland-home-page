import styles from "./testimonials.module.scss";
import Card from "./cardSlider/Card";
import Image from "next/image";

const Testimonials = () => {
  return (
    <div className={`${styles.testimonials} common_margin`}>
      <h3 className="commonBigHeadingStyle">Testimonial</h3>
      {/* <span style={{position:"absolute" ,left:"0"}}><Image src="/icons/testimonials_before.svg" alt="quotes" width={180} height={160} /></span> */}
      <div className={`commonParagraphStyle ${styles.testimonialsHeadingPara}`}>
        Lorem ipsum dolor sit amet consectetur. Eu tortor dolor donec urna
       amet. Commodo dignissim tellus interdum vivamus aliquet in.
      </div>
      {/* <span style={{position:"absolute" ,right:"0"}}><Image src="/icons/testimonials_after.svg" alt="quotes" width={180} height={160} /></span> */}
      <div className="container">
        <Card />
      </div>
    </div>
  );
};

export default Testimonials;
