"use client";
import styles from "./chooseCountry.module.scss";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Image from "next/image";
// import "./Slider.css";

const ChooseCountry = () => {

  const settings = {
    dots: false,
    infinite: true,
    speed: 1000,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 3000,
    responsive: [

      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 2.5,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 400,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 340,
        settings: {
          slidesToShow: 1.8,
          slidesToScroll: 1,
        }
      }
    ]
  };

  const countries = [
    {
      title: 'Canada',
      image: '/vectors/studyin-canada.png'
    },
    {
      title: 'USA',
      image: '/vectors/studyin-usa.png'
    },
    {
      title: 'Australia',
      image: '/vectors/studyin-australia.png'
    },
    {
      title: 'Germany',
      image: '/vectors/studyin-germany.png'
    },
    {
      title: 'Singapore',
      image: '/vectors/studyin-singapore.png'
    },
    {
      title: 'Uk',
      image: '/vectors/studyin-uk.png'
    },

  ];

  return (
    <div className={`${styles.ChooseCountryContainer} common_margin`}>
      <div className="container">
        <div className="row no-overflow" >
          <div className={`col-lg-6 col-md-6 col-sm-12 pe-5 ${styles.choose_country_child}`}>
            <h5 className={`commonSmallHeadingStyle ${styles.ChooseCountrySmallHeading}`}>CHOOSE COUNTRY</h5>
            <h4 className={`commonBigHeadingStyle ${styles.ChooseCountryBigHeading}`}>Want to study in?</h4>
            <p className="commonParagraphStyle mt-3">
              Lorem ipsum dolor sit amet consectetur. Eu tortor dolor donec urna
              amet. Commodo dignissim tellus interdum vivamus aliquet in.
            </p>
          </div>

          <div className="col-lg-6 col-md-6 col-sm-12">
            <Slider {...settings}>
              {countries.map((country, index) => (
                <div key={index} className={styles.choose_country_child} style={{ outline: 'none' }}>
                  <div> <Image src={country.image} height={250} width={180} alt={country.title} /></div>
                  <p className={styles.country_title}>{country.title}</p>
                </div>
              ))}
            </Slider>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ChooseCountry;
