"use client";
import Image from "next/image";
import styles from "./keepUpdated.module.scss";
import "./buttonStyle.scss";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const data = [
  {
    imageSrc: "/visaType/canadaVisa.png",
    title: "How to choose a university in Canada",
    ribbonData: "Visa support",
    description:
      "Finding the ideal university or college for you to study abroad may be exhausting & lead to uncertainty in the student’s mind. The awesome thing is....",
  },
  {
    imageSrc: "/visaType/canadavisaStudent.png",
    title: "The Best Scholarships for Indian Students in Canada",
    ribbonData: "IELTS and Visa support",
    description:
      "To assist excellent overseas students in funding their education, Canadian universities & colleges provide a variety of scholarships. Although Canada...",
  },
  {
    imageSrc: "/visaType/usaVisa.png",
    title: "How to choose a university in the USA",
    ribbonData: "IELTS and Visa support",
    description:
      "Due to the abundance of options & some of the finest facilities available elsewhere in the globe, the US has drawn millions of international students to its institutions & colleges. The...",
  },
  {
    imageSrc: "/visaType/canadaVisa.png",
    title: "How to choose a university in Canada",
    ribbonData: "IELTS and Visa support",
    description:
      "Finding the ideal university or college for you to study abroad may be exhausting & lead to uncertainty in the student’s mind. The awesome thing is....",
  },
  {
    imageSrc: "/visaType/canadavisaStudent.png",
    title: "The Best Scholarships for Indian Students in Canada",
    ribbonData: "IELTS and Visa support",
    description:
      "To assist excellent overseas students in funding their education, Canadian universities & colleges provide a variety of scholarships. Although Canada...",
  },
  {
    imageSrc: "/visaType/usaVisa.png",
    title: "How to choose a university in the USA",
    ribbonData: "IELTS and Visa support",
    description:
      "Due to the abundance of options & some of the finest facilities available elsewhere in the globe, the US has drawn millions of international students to its institutions & colleges. ",
  },
];

const KeepUpdated = () => {

  const settings = {
    className: "center",
    // centerMode: true,
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          centerMode: false, // Remove centerMode for smaller screens
        },
      },
      {
        breakpoint: 646,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: false, // Remove centerMode for even smaller screens
        },
      },
    ],
  };

  const truncateDescription = (description, maxCharacters) => {
    if (description.length > maxCharacters) {
      return description.slice(0, maxCharacters) + '...';
    }
    return description;
  }


  return (
    <section className="common_margin">
      <div className="container">
        <h3 className="commonBigHeadingStyle mb-3" style={{ textAlign: "center" }}>
          Keep Updated
        </h3>
        <p className="commonParagraphStyle" style={{ textAlign: "center" }}>
          Stay Informed and Inspired: Explore Our Insightful Blogs on Immigration
          <br />
          Trends, Visa Updates, and Expert Advice!
        </p>
        <div>
          <Slider {...settings} className="blogs_slider">
            {data.map((item, index) => (
              <div key={index}>
                <div className={`${styles.imageContainer} `}>
                  <span className="ribbon">{item.ribbonData}</span>
                  <div className={styles.image_main}>
                    <Image
                      src={item.imageSrc}
                      fill={true}
                      alt=""
                    />
                  </div>
                  <h5>{item.title}</h5>
                  <p>{truncateDescription(item.description, 100)}</p>
                  <button
                    className='circle-button'
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="21"
                      height="21"
                      viewBox="0 0 21 21"
                      fill="none"
                    >
                      <path
                        d="M12.2627 3.86003L17.9757 9.63806C18.0645 9.72753 18.1347 9.83365 18.1824 9.95031C18.2301 10.067 18.2542 10.1919 18.2535 10.318C18.2528 10.444 18.2272 10.5686 18.1782 10.6848C18.1292 10.8009 18.0578 10.9062 17.968 10.9946L12.19 16.7076C12.0091 16.8865 11.7645 16.9862 11.5101 16.9848C11.2557 16.9833 11.0123 16.8809 10.8334 16.7C10.6545 16.5191 10.5549 16.2745 10.5563 16.0201C10.5577 15.7657 10.6602 15.5223 10.8411 15.3434L14.9748 11.2562L3.24436 11.1898C2.9904 11.1884 2.7474 11.0861 2.56883 10.9055C2.39027 10.7249 2.29076 10.4808 2.2922 10.2268C2.29363 9.97284 2.3959 9.72985 2.5765 9.55128C2.7571 9.37271 3.00124 9.2732 3.2552 9.27464L14.9856 9.34104L10.8977 5.20733C10.7188 5.02642 10.6191 4.78186 10.6205 4.52745C10.622 4.27304 10.7244 4.02962 10.9053 3.85075C11.0863 3.67187 11.3308 3.57219 11.5852 3.57363C11.8396 3.57507 12.083 3.67751 12.2619 3.85843L12.2627 3.86003Z"
                        fill="white"
                      />
                    </svg>
                  </button>
                </div>
              </div>
            ))}
          </Slider>
        </div >
      </div>
    </section>

  );
};

export default KeepUpdated;
