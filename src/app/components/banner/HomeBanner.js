"use client";
import Image from "next/image";
import styles from "./HomeBanner.module.scss";
import Slider from "react-slick";
import { useEffect, useRef, useState } from "react";

export default function HomeBanner() {
  const sliderRef = useRef(null);

  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 5000,
    // draggable: false,
  };

  const banners = [
    {
      image_url_desktop: "/banners/home-banner-one.png",
      image_url_mobile: "/banners/mobile-banner-one.png",
      sub_title: "Unlock Your",
      title: "Future POTENTIAL",
      lists: [
        'Study Visa',
        'Tourist Visa',
        'Job Seeker Visa',
        'Permanent Residency (PR)'
      ]
    },
    {
      image_url_desktop: "/banners/home-banner-two.png",
      image_url_mobile: "/banners/mobile-banner-two.png",
      sub_title: "Begin Your",
      title: "IELTS Coaching Journey Today!",
      lists: [
        'Expert Guidance',
        'Flexible Timings',
        'Individual Attention',
        'Result Driven Coaching'
      ]

    },
    {
      image_url_desktop: "/banners/home-banner-three.png",
      image_url_mobile: "/banners/mobile-banner-three.png",
      sub_title: "Thinking About",
      title: "Study in USA?",
      lists: [
        'We ve got your back!',
        'Let us guide you every step of the way!',
      ]
    }
  ];

  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    setIsMobile(window.innerWidth <= 768);

    const handleResize = () => {
      setIsMobile(window.innerWidth <= 768);
    };

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return (
    <>
      <Slider ref={sliderRef} {...settings}>
        {banners.map((banner, index) => (
          <div key={index}>
            <div className={styles.bannerImage}>
              <div className="container">
                <div className=" flex align_items_center">
                  <div className={styles.banner_content}>
                    <h4>{banner.sub_title}</h4>
                    <h2>{banner.title}</h2>
                    <ul>
                      {banner.lists.map((list, index) => (
                        <li key={index}>{list}</li>
                      ))}
                    </ul>
                  </div>
                </div>
              </div>
              {isMobile ? (
                <Image src={banner.image_url_mobile} alt={`banner${index + 1}`} fill={true} />)
                : <Image src={banner.image_url_desktop} alt={`banner${index + 1}`} fill={true} />}
            </div>
          </div>
        ))}
      </Slider>
    </>
  );
}