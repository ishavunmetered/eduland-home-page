"use client"
import styles from "./universityPartners.module.scss";
import Image from "next/image";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export const UniversityPartners = () => {

  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 6, 
    slidesToScroll: 1, 
    autoplay: true,
    arrows: false,
    responsive: [
      {
        breakpoint: 1024, 
        settings: {
          slidesToShow: 4,
        },
      },
      {
        breakpoint: 768, 
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 480, 
        settings: {
          slidesToShow: 1,
          className: "center",
          centerMode: true,
        },
      },
    ],
  };
  
  return (
    <div className={`${styles.universityPartners} common_margin`} >
        <h3 className="commonBigHeadingStyle" style={{ textAlign: "center" }}>Our Top University Partners</h3>
        <div className={styles.logos_main} style={{padding:"0 0 0 10px"}}>
        <Slider {...settings} >
          <div ><Image src="/universityImages/cape.png"  width={173} height={100} alt="Error" /></div>
          <div><Image src="/universityImages/aldoma.png"  width={173} height={100} alt="Error" /> </div>
          <div><Image src="/universityImages/capilano.png" width={173} height={100}  alt="Error" /></div>
          <div><Image src="/universityImages/flwming.png"  width={173} height={100}alt="Error" /></div>
          <div><Image src="/universityImages/st.clare.png" width={173} height={100} alt="Error" /></div>
          <div><Image src="/universityImages/st.lawrence.png"  width={173} height={100} alt="Error" /></div>
          <div><Image src="/universityImages/st.lawrence.png" width={173} height={100} alt="Error" /></div>
          </Slider>
        </div>
    </div>
  )
}
