
'use client';
import styles from './LogoRotator.module.scss'

const LogoRotator = () => {

    return (
        <div className={styles.main_cont}>
            <div className={styles.rotating_box}>
                {/* <div className={styles.logo_container}> */}
                <span className={`${styles.logo1} ${styles.logo}`}>Logo 1</span>
                <span className={`${styles.logo2} ${styles.logo}`}>Logo 2</span>
                <span className={`${styles.logo3} ${styles.logo}`}>Logo 3</span>
                <span className={`${styles.logo4} ${styles.logo}`}>Logo 4</span>
                {/* </div> */}
            </div>
        </div>

    )
};

export default LogoRotator


// const logos = [
//     '/images/wisconsin.png',
//     '/images/wisconsin.png',
//     '/images/wisconsin.png',
//     '/images/wisconsin.png',
//     '/images/wisconsin.png',
// ];
