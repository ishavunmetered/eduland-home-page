"use client";
import Slider from "react-slick";
import styles from "./visaHolders.module.scss";
import Image from "next/image";
const VisaHolders = () => {

  const visaHoldersData = [
    {
      name: "Ms Sulendra",
      visaType: "Student Visa (Canada)",
      imagePath: "/recentVisaHolders/Ms_Sulendra_Canada_Study_Visa.png",
    },
    {
      name: "Mr Akash ",
      visaType: "Student Visa (USA)",
      imagePath: "/recentVisaHolders/Mr_Akash_USA_Study_Visa.png",

    },
    {
      name: "Mr Harjinder Singh",
      visaType: "Tourist Visa (Canada)",
      imagePath: "/recentVisaHolders/Mr_Harjinder_Singh_Canada_Tourist_Visa.png",

    },
    {
      name: "Novedeep Kaur",
      visaType: "Student Visa (USA)",
      imagePath: "/recentVisaHolders/USA_Study_Visa_Novedeep_Kaur.png",

    },
    {
      name: "Ms Sulendra",
      visaType: "Student Visa (Canada)",
      imagePath: "/recentVisaHolders/Ms_Sulendra_Canada_Study_Visa.png",
    },
    {
      name: "Mr Akash ",
      visaType: "Student Visa (USA)",
      imagePath: "/recentVisaHolders/Mr_Akash_USA_Study_Visa.png",

    },
    {
      name: "Mr Harjinder Singh",
      visaType: "Tourist Visa (Canada)",
      imagePath: "/recentVisaHolders/Mr_Harjinder_Singh_Canada_Tourist_Visa.png",

    },
    {
      name: "Novedeep Kaur",
      visaType: "Student Visa (USA)",
      imagePath: "/recentVisaHolders/USA_Study_Visa_Novedeep_Kaur.png",

    },

  ];

  const sliderSettings = {
    dots: false,
    infinite: true,
    speed: 100,
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: true,
    arrows: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  };

  return (
    <>
      <div className={`${styles.visaHolders} common_margin`}>
        <div>
          <h3
            className="commonBigHeadingStyle "
            style={{ textAlign: "center" }}
          >Ready to take the Leap?</h3>
          <p
            className="commonParagraphStyle py-3"
            style={{ textAlign: "center" }}
          >A few of our recent visa holders</p>
        </div>

        <Slider {...sliderSettings} className={styles.visaHoldersSlider}>
          {visaHoldersData.map((holder, index) => (
            <div key={index}>
              <div className={styles.userImage} style={{ paddingBottom: "0px", margin: "0" }}>
                <Image
                  src={holder.imagePath}
                  alt="user image"
                  fill={true}
                />
                <div className={styles.userText}>
                  <p><span style={{ fontWeight: "bold" }}>{holder.name}</span></p>
                  <p>{holder.visaType}</p>
                </div>
              </div>
            </div>
          ))}
        </Slider>
      </div>
    </>
  );
};
export default VisaHolders;