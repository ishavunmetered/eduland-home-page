import styles from "./footer.module.scss";
import Image from "next/image";
import Link from "next/link";
const Footer = () => {
  return (
    <>
      <footer className={styles.footerContainer} style={{ zIndex: "-1" ,marginTop:"-6px"}}>
        <div className="container py-4">
          <div className="footerSocialLinksContainerGlobal">
            <div className="footerSocialLinksAddressGlobal ">
            <Link href={"/"}>
            <Image
                src="/logo/logo.svg"
                alt="Logo"
                width={156.615}
                height={37}
              />
            </Link>
              <p className="pt-4 pb-2 m-0 footerAddress">
                <Link href="tel:+918348312345" className="footerAnchorStyle">
                  +91 83483 12345
                </Link>{" "}
              </p>
              
                <Link href={`mailto:contact@edulandimmigration.com`} className="pt-0 pb-2 m-0 footerAddress footerAnchorStyle">
                  contact@edulandimmigration.com
                </Link>
            
              <div>
                <button className={`commonButtonStyle ${styles.footerBtn}`}>
                  Book Consultant
                </button>
              </div>
            </div>
            <div className={`footerSocialLinksGlobal ${styles.socialLinksContainer}`}>
              <h3 className="footerHeadingStyle">Links</h3>
              <Link href="#" className="footerAnchorStyle">
                About Us
              </Link>
              <Link href="#" className="footerAnchorStyle">
                Services
              </Link>

              <Link href="#" className="footerAnchorStyle">
                Countries
              </Link>

              <Link href="#" className="footerAnchorStyle">
                Courses
              </Link>

              <Link href="#" className="footerAnchorStyle">
                Blog
              </Link>

              <Link href="#" className="footerAnchorStyle">
                Contact Us
              </Link>
            </div>
            <div className={`footerSocialLinksGlobal ${styles.socialLinksContainer}`}>
              <h3 className="footerHeadingStyle">Countries</h3>
              <Link href="#" className="footerAnchorStyle">
                USA
              </Link>
              <Link href="#" className="footerAnchorStyle">
                Canada
              </Link>

              <Link href="#" className="footerAnchorStyle">
                UK
              </Link>

              <Link href="#" className="footerAnchorStyle">
                Australia
              </Link>

              <Link href="#" className="footerAnchorStyle">
                Singapore
              </Link>

              <Link href="#" className="footerAnchorStyle">
                Germany
              </Link>
            </div>

            <div className={`footerSocialLinksGlobal ${styles.socialLinksContainer}`}>
              <h3 className="footerHeadingStyle">Courses</h3>
              <Link href="#" className="footerAnchorStyle">
                IELTS
              </Link>
              <Link href="#" className="footerAnchorStyle">
                PTE
              </Link>

              <Link href="#" className="footerAnchorStyle">
                GRE
              </Link>

              <Link href="#" className="footerAnchorStyle">
                TOEFL
              </Link>

              <Link href="#" className="footerAnchorStyle">
                GMAT
              </Link>

              <Link href="#" className="footerAnchorStyle">
                SAT
              </Link>
            </div>
          </div>

          <hr className="w-100 color-light my-4" />

          <div className="container-fluid">
            <div className="row">
              <div className="col-lg-6 col-md-6 col-sm-12 px-0"></div>

              <div className="col-lg-6 col-md-6 col-sm-12 px-0">
                <div className="container-fluid">
                  <div className="row ">
                    <div
                      className={`col-lg-6 col-md-6 col-sm-12 px-0  ${styles.addressContainer}`}
                    >
                      <h3 className="footerHeadingStyle">Reach us</h3>
                      <p className="footerAddress">
                        Eduland Immigration 1&2 Floor SCO 81, Sector 40C{" "}
                        Chandigarh, PIN : 160036
                      </p>
                    </div>
                    <div
                      className={`col-lg-6 col-md-6 col-sm-12 px-0  ${styles.addressContainer}`}
                    >
                      <h3 className="footerHeadingStyle">Office timing</h3>
                      <p className="footerAddress">
                        Monday-Friday: 9:30 am - 6:00 pm Saturday: 9:30 am -
                        5:00 pm
                      </p>
                      <h3
                        style={{ margin: "15px 0px 15px 0" }}
                        className=" footerHeadingStyle"
                      >
                        {" "}
                        Follow us on
                      </h3>
                      <div className={styles.followUsContainer}
                       
                      >
                        <Link href="https://www.facebook.com/edulandimmigration">
                          <div className={styles.SocialLinksCircle}>
                            <Image
                              src="/icons/facebook.svg"
                              width={14}
                              height={14}
                              alt="facebook"
                            />
                          </div>
                        </Link>
                        <Link href="https://www.instagram.com/edulandimmigration/">
                          <div className={styles.SocialLinksCircle}>
                            <Image
                              src="/icons/insta.svg"
                              width={14}
                              height={14}
                              alt="facebook"
                            />
                          </div>
                        </Link>
                        <Link href="https://www.youtube.com/channel/UC1xB2xRU0B1i01JOygRTAmg">
                          <div className={styles.SocialLinksCircle}>
                            <Image
                              src="/icons/youtube.svg"
                              width={14}
                              height={14}
                              alt="facebook"
                            />
                          </div>
                        </Link>
                        <Link href="https://www.linkedin.com/in/edulandimmigration">
                          <div className={styles.SocialLinksCircle}>
                            <Image
                              src="/icons/linkdin.svg"
                              width={14}
                              height={14}
                              alt="facebook"
                            />
                          </div>
                        </Link>

                        <Link href="#">
                          <div className={styles.SocialLinksCircle}>
                            <Image
                              src="/icons/telegram.svg"
                              width={14}
                              height={14}
                              alt="facebook"
                            />
                          </div>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className={styles.footerCopyright}>
            <p className={styles.footerCopyrightText}>
              Copyright 2023 All Rights Reserved.&nbsp; 
              <Link href={"/"}>Privacy Policy</Link> {""}|&nbsp;&nbsp;
              <Link href={"/"}>Cancellation Policy</Link> {""}|&nbsp;&nbsp;
              <Link href={"/"}>Terms & Condition</Link>
            </p>
            <p>Powered by Unmetered</p>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Footer;
