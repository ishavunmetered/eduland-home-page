"use client";
import { useState } from "react";
import styles from "./VisaSteps.module.scss";
import Image from "next/image";

const stepsData = [
  {
    title: "Know your visa type",
    description: "Step 1: Know your visa type",
    image: "/images/visa-step-1.png",
  },
  {
    title: "Complete your application",
    description: "Step 2: Complete your application",
    image: "/images/visa-step-2.png",
  },
  {
    title: "Pay your visa fees",
    description: "Step 3: Pay your visa fees",
    image: "/images/visa-step-3.png",
  },
  {
    title: "Schedule your Appointment",
    description: "Step 4: Schedule your Appointment",
    image: "/images/visa-step-4.png",
  },
  {
    title: "Attend your Interview",
    description: "Step 5: Attend your Interview",
    image: "/images/visa-step-5.png",
  },
  {
    title: "Track your Passport",
    description: "Step 6: Track your Passport",
    image: "/images/visa-step-6.png",
  },
];

const VisaSteps = () => {
  const [currentStep, setCurrentStep] = useState(1);

  const handleClick = (step) => {
    setCurrentStep(step);
  };

  const progress = ((currentStep / stepsData.length) * 100 - 5);

  return (
    <section className={`${styles.visaSteps} common_margin`} >
      <div className="container">
        <div className="row">
          <div className="col-lg-6 col-md-12 col-sm-12" >
            <div className={styles.visasteps_image}>
              <Image
                src={stepsData[currentStep - 1].image}
                fill={true}
                alt="Visa Step"
              />
            </div>

          </div>
          <div className="col-lg-6 col-md-12 col-sm-12">
            <div className={`mb-2 ps-5 ${styles.visaStepsHeadingContainer} `}>
              <h4 className={`commonSmallHeadingStyle `}>With In 6 Steps</h4>
              <h2 className={`commonBigHeadingStyle `}>Get Your Visa</h2>
            </div>
            <div className="row">
              <div className="col-1" >
                <div className={styles.progressBarContainer}>
                  <div
                    className={styles.progressBar}
                    style={{ height: `${progress}%` }}
                  >
                    <div className={styles.progress_bar_dot} style={{ position: "absolute", left: "-5px", top: ` ${progress}%` }}>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="13"
                        height="13"
                        viewBox="0 0 13 13"
                        fill="none"
                      >
                        <path
                          d="M13 6.5C13 10.0899 10.0899 13 6.5 13C2.91015 13 0 10.0899 0 6.5C0 2.91015 2.91015 0 6.5 0C10.0899 0 13 2.91015 13 6.5Z"
                          fill="#E0EBF6"
                        />
                      </svg>
                    </div>
                  </div>
                </div>
              </div>
              <div className={`${styles.VisaTYpeButton_main} col-11`}>
                {stepsData.map((step, index) => (
                  <div key={index} className={styles.VisaTYpeButton_main_div}>
                    <button
                      onClick={() => handleClick(index + 1)}
                      className={`${styles.visaTypeButton} ${currentStep === index + 1 ? styles.active : ""
                        }`}
                    >
                      {step.description}
                    </button>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default VisaSteps;
